terraform {
  backend "s3" {
    bucket         = "gyko-infra"
    key            = "ecs.tfstate"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform_lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.0"
    }
  }
}


provider "aws" {
  region  = "ap-northeast-2"
  profile = "default"
}


resource "aws_ecs_cluster" "gyko_ecs_cluster" {
  name = "gyko_ecs_cluster"
}
