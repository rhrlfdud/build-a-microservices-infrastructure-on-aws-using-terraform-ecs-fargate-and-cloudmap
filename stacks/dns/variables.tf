variable "gyko_private_dns_namespace" {
  description = "gyko private dns namespace"
  default     = "gyko-app.test"
}
