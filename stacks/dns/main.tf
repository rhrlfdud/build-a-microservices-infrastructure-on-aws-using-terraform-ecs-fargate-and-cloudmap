terraform {
  backend "s3" {
    bucket         = "gyko-infra"
    key            = "dns.tfstate"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform_lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.0"
    }
  }
}


provider "aws" {
  region  = "ap-northeast-2"
  profile = "default"
}



data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "gyko-infra"
    key    = "vpc.tfstate"
    region = "ap-northeast-2"
  }
}


resource "aws_service_discovery_private_dns_namespace" "gyko_dns_discovery" {
  name        = var.gyko_private_dns_namespace
  description = "gyko dns discovery"
  vpc         = data.terraform_remote_state.vpc.outputs.gyko_vpc_id
}
