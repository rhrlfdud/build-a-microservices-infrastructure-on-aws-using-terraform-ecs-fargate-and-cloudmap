output "gyko_dns_discovery_id" {
  description = "gyko service discovery id"
  value       = aws_service_discovery_private_dns_namespace.gyko_dns_discovery.id
}

output "gyko_private_dns_namespace" {
  description = "gyko service discovery id"
  value       = var.gyko_private_dns_namespace
}
