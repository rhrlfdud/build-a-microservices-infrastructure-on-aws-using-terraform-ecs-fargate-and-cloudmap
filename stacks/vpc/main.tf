terraform {
  backend "s3" {
    bucket         = "gyko-infra"
    key            = "vpc.tfstate"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform_lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.0"
    }
  }
}


provider "aws" {
  region  = "ap-northeast-2"
  profile = "default"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "gyko-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["ap-northeast-2a", "ap-northeast-2c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    group       = "gyko"
    Environment = "dev"
  }
}
