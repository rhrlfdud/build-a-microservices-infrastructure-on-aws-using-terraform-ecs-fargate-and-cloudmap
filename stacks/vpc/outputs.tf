output "gyko_vpc_id" {
  description = "gyko subnet private 1"
  value       = module.vpc.vpc_id
}

output "gyko_private_subnets_ids" {
  description = "gyko private subnets ids"
  value       = module.vpc.private_subnets
}

output "gyko_public_subnets_ids" {
  description = "gyko public subnets ids"
  value       = module.vpc.public_subnets
}

