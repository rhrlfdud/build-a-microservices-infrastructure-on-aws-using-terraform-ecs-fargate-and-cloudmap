output "gyko_alb_sg_id" {
  description = "gyko private subnets ids"
  value       = aws_security_group.gyko_alb_sg.id
}

output "gyko_alb_id" {
  description = "gyko public alb"
  value       = aws_lb.gyko_alb.id
}

output "gyko_alb_dns_name" {
  description = "gyko public alb dns name"
  value       = aws_lb.gyko_alb.dns_name
}

output "gyko_alb_zone_id" {
  description = "gyko public alb zone id"
  value       = aws_lb.gyko_alb.zone_id
}

