terraform {
  backend "s3" {
    bucket         = "gyko-infra"
    key            = "ecr.tfstate"
    region         = "ap-northeast-2"
    dynamodb_table = "terraform_lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.0.0"
    }
  }
}


provider "aws" {
  region  = "ap-northeast-2"
  profile = "default"
}



resource "aws_ecr_repository" "gyko-uno" {
  name = "gyko-uno"
}

resource "aws_ecr_repository" "gyko-due" {
  name = "gyko-due"
}

resource "aws_ecr_repository" "gyko-tre" {
  name = "gyko-tre"
}
